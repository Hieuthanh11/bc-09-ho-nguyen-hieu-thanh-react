import React, { Component } from "react";
import Slide1 from "../../assets/img/slide_1.jpg";
import Slide2 from "../../assets/img/slide_1.jpg";
import Slide3 from "../../assets/img/slide_1.jpg";
class Slider extends Component {
  render() {
    return (
      <div id="demo" className="carousel slide" data-ride="carousel">
        <ul className="carousel-indicators">
          <li data-target="#demo" data-slide-to={0} className="active" />
          <li data-target="#demo" data-slide-to={1} className />
          <li data-target="#demo" data-slide-to={2} className />
        </ul>
        <div className="carousel-inner">
          <div className="carousel-item active">
            <img className="w-100" src={Slide1} alt="Los Angeles" />
          </div>
          <div className="carousel-item">
            <img className="w-100" src={Slide2} alt="Chicago" />
          </div>
          <div className="carousel-item">
            <img className="w-100" src={Slide3} alt="New York" />
          </div>
        </div>
        <a className="carousel-control-prev" href="#demo" data-slide="prev">
          <span className="carousel-control-prev-icon" />
        </a>
        <a className="carousel-control-next" href="#demo" data-slide="next">
          <span className="carousel-control-next-icon" />
        </a>
      </div>
    );
  }
}

export default Slider;
