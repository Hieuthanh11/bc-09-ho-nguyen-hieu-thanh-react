import React, { Component } from "react";
import Laptop from "./laptop";
import Phone from "./phone";

class ProductList extends Component {
  render() {
    return (
      <div>
        <Phone />
        <Laptop />
      </div>
    );
  }
}

export default ProductList;
