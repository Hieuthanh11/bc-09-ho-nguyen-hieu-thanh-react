import React, { Component } from "react";
import Footer from "./footer";
import Header from "./header";
import ProductList from "./productList";
import Slider from "./slider";

class Home extends Component {
  render() {
    return (
      <div>
        <Header />
        <Slider />
        <ProductList />
        <Footer />
      </div>
    );
  }
}

export default Home;
