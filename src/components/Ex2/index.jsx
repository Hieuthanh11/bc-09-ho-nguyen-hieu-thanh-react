import React, { Component } from "react";
import Content from "./content";
import Footer from "./footer";
import Header from "./header";
import Slider from "./slider";

class Index extends Component {
  render() {
    return (
      <div>
        <Header />
        <Slider />
        <Content />
        <Footer />
      </div>
    );
  }
}

export default Index;
